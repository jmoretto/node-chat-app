// [{
//   id: 'fasdfasdf',
//   name: 'Test user',
//   room: 'Test room'
// }]

// addUser(id, name, room)
// removeUser(id)
// getUser(id)
// getUserList(room)

class Users {
  constructor () {
    this.users = []
  }

  addUser (id, name, room) {
    const user = {id, name, room}
    this.users.push(user)
    return user
  }

  removeUser (id) {
    // return user that was removed
    let userRemoved
    this.users.slice().forEach((user, idx) => {
      if (user.id === id) {
        this.users.splice(idx, 1)
        userRemoved = user
      }
    })

    return userRemoved
  }

  getUser (id) {
    return this.users.find((user) => user.id === id)
  }

  getUserList (room) {
    const users = this.users.filter((user) => user.room === room)
    return users.map((user) => user.name)
  }
}

module.exports = {
  Users
}

// class Person {
//   constructor (name, age) {
//     this.name = name
//     this.age = age
//   }

//   getUserDescription() {
//     return `${this.name} is ${this.age} year(s) old.`
//   }
// }

// const me = new Person('Test', 23)
// console.log('this.name', me.name)
// console.log('this.age', me.age)
// const description = me.getUserDescription()
// console.log('description', description)