const expect = require('expect')
const { isRealString } = require('./validation')

describe('validation', () => {
  it('should reject non-string values', () => {
    expect(isRealString(1)).toEqual(false)
  })

  it('should reject string with only spaces', () => {
    expect(isRealString('    ')).toEqual(false)
  })

  it('should allow strings with non-space characters', () => {
    expect(isRealString('test')).toEqual(true)
  })
})
