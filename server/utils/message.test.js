const expect = require('expect')

const { generateMessage, generateLocationMessage } = require('./message')

describe('generateMessage', () => {
  it('should generate correct message object', () => {
    const from = 'test'
    const text = 'Some message'
    const res = generateMessage(from, text)
    expect(res.from).toBe(from)
    expect(res.text).toBe(text)
    expect(typeof res.createdAt).toBe('number')
  })
})

describe('generateLocationMessage', () => {
  it('should generate correct location object', () => {
    const from = 'test'
    const res = generateLocationMessage(from, 1, 1)
    expect(res.from).toBe(from)
    expect(res.url).toBe(`https://www.google.com/maps?q=1,1`)
    expect(typeof res.createdAt).toBe('number')
  })
})