// Root of the node server.
const path = require('path')
const http = require('http')
const express = require('express')
const socketIO = require('socket.io')

const { generateMessage, generateLocationMessage } = require('./utils/message')
const { isRealString } = require('./utils/validation')
const { Users } = require('./utils/users')
const publicPath = path.join(__dirname, '../public')
const port = process.env.PORT || 3000

// Initialize exppress server
const app = express()
const server = http.createServer(app)

// Websocket server
const io = socketIO(server)
const users = new Users()

// Configure middleware
app.use(express.static(publicPath))

io.on('connection', (socket) => {
  // Socket represents an individual socket
  console.log('New user connected')

  socket.on('join', (params, callback) => {
    if (!isRealString(params.name) || !isRealString(params.room)) {
      return callback('Name and room name are required.')
    }

    // Use socket io rooms API
    socket.join(params.room)
    // socket.leave(name) // leave the room
    users.removeUser(socket.id)
    users.addUser(socket.id, params.name, params.room)

    io.to(params.room).emit('updateUserList', users.getUserList(params.room))

    // io.emit -> io.to()
    // socket.broadcast.emit -> socket.broadcast.to().emit
    // socket.emit

    socket.emit('newMessage', generateMessage('Admin', 'Welcome to the chat app'))

    socket.broadcast.to(params.room).emit('newMessage', generateMessage('Admin', `${params.name} joins`))

    callback()
  })

  socket.on('createMessage', (message, callback) => {
    const user = users.getUser(socket.id)

    if (user && isRealString(message.text)) {
      io.to(user.room).emit('newMessage', generateMessage(user.name, message.text))
    }

    // Emits an event to every single connection.
    callback()

    // Sends event to everybody but this socket.
    // socket.broadcast.emit('newMessage', {
    //   ...message,
    //   createdAt: new Date().getTime()
    // })
  })

  socket.on('createLocationMessage', (coords) => {
    const user = users.getUser(socket.id)

    if (user) {
      io.to(user.room).emit('newLocationMessage', generateLocationMessage(user.name, coords.latitude, coords.longitude))
    }
  })

  socket.on('disconnect', () => {
    const user = users.removeUser(socket.id)

    if (user) {
      io.to(user.room).emit('updateUserList', users.getUserList(user.room))
      io.to(user.room).emit('newMessage', generateMessage('Admin', `${user.name} has left`))
    }
  })
})

// Set server to listen on designated port
server.listen(port, () => {
  console.log(`listening on port ${port}`)
})
