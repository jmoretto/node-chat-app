// Jan 1st 1970 00:00:00 am

const moment = require('moment')

// const date = new Date()
// const month = ['Jan', 'Feb'] //...
// console.log('date.getMonth()', date.getMonth())

const createdAt = 1234
const date = moment(createdAt)
// console.log('date.format()', date.format('MMM Do, YYYY'))

// 10:35 am
// 6:01 am
console.log('date.format()', date.format('h:mm a'))